#Build stage
FROM node:8.16.0-alpine as build

RUN mkdir -p /home/deploy/contacts-importer/node_modules/
ADD package.json /home/deploy/contacts-importer/package.json

RUN cd /home/deploy/contacts-importer/ && npm install --no-package-lock --only=production

#Deploy stage
FROM mhart/alpine-node:slim-8.16.0

RUN mkdir -p /home/deploy/contacts-importer/node_modules/ \
    && mkdir -p /home/deploy/contacts-importer/logs/ \
    && apk update \
    && apk add --no-cache wget

COPY --from=build /home/deploy/contacts-importer/node_modules /home/deploy/contacts-importer/node_modules

WORKDIR /home/deploy/contacts-importer
ADD contacts.js /home/deploy/contacts-importer/contacts.js
ADD core /home/deploy/contacts-importer/core/
ADD public /home/deploy/contacts-importer/public/

#zk segregation pre requisites
RUN apk add --no-cache python && python -m ensurepip && pip install kazoo
COPY configfiles/docker/zookeeper.json /home/deploy/docker/zookeeper.json
COPY configfiles/docker/zookeeper_k8s.json /home/deploy/docker/zookeeper_k8s.json
COPY configfiles/docker/init.sh /usr/local/scripts/init.sh
COPY configfiles/docker/startup.sh /home/deploy/docker/startup.sh
RUN chmod +x /usr/local/scripts/init.sh
RUN chmod +x /home/deploy/docker/startup.sh

EXPOSE 5007

ARG ZK_URL
ARG ZK_ROOT=FE
ARG NODE_ENV
ENV NODE_ENV=${NODE_ENV}
ENV ZK_ROOT=${ZK_ROOT}
ENV ZK_URL=${ZK_URL}

#CMD [ "node", "contacts.js" ]
ENTRYPOINT ["/bin/sh","/home/deploy/docker/startup.sh"]