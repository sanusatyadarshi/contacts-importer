var express = require('express');
var path = require('path');
var cluster = require('cluster');
var mLogger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var logger = require('./core/utils/loggerutil');
var configMaker = require('./core/zk/configMaker');

if(cluster.isMaster)
{
    var numCPUs = require('os').cpus().length;
    // numCPUs = numCPUs > 8 ? 8 : numCPUs;
    if(process.env.NODE_ENV === "development") numCPUs = 1;
    for(var i = 0; i < numCPUs; i++)
    {
        cluster.fork();
    }
    cluster.on('exit', function (worker, code, signal)
    {
        //Cluster worker exited.
        logger.fatal(
        {
            error: 'Worker EXITED!',
            worker: worker,
            code: code,
            signal: signal
        }, "Cluster Worker EXITED due to some error!");
        cluster.fork();
    })
    cluster.on('disconnect', function (worker)
    {
        logger.fatal(
        {
            error: 'Worker DISCONNECTED!',
            worker: worker
        }, "Cluster Worker DISCONNECTED due to some error!");
    });
}
else
{
    configMaker.init(function ()
    {
        var AppConfig = require("./core/config/appconfig");

        if (AppConfig.dnscacheProps && AppConfig.dnscacheProps.enable) {
            require("dnscache")({
                enable: AppConfig.dnscacheProps.enable,
                ttl: AppConfig.dnscacheProps.ttl || 5,
                cachesize: AppConfig.dnscacheProps.cachesize || 1000
            });
        }
        var routes = require('./core/routes/index');
        var app = express();

        // uncomment after placing your favicon in /public
        //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
        app.use(mLogger('dev'));
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded(
        {
            extended: false
        }));
        app.use(cookieParser());
        app.use(express.static(path.join(__dirname, 'public')));
        app.use('/', routes);
        // catch 404 and forward to error handler
        app.use(function (req, res, next)
        {
            var err = new Error('Not Found');
            err.status = 404;
            logger.error(err, 'Resource not found.');
            next(err);
        });
        // error handlers
        // development error handler
        // will print stacktrace
        if(app.get('env') === 'development')
        {
            app.use(function (err, req, res, next)
            {
                logger.error(err, 'Internal server error');
                res.status(err.status || 500);
                res.render('error',
                {
                    message: err.message,
                    error: err
                });
            });
        }
        // production error handler
        // no stacktraces leaked to user
        /*app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: {}
            });
        });*/
        //Get port from environment and store in Express.
        var port = normalizePort(process.env.PORT || '5007');
        app.set('port', port);
        //Create HTTP server.
        var server = http.createServer(app);
        //Listen on provided port, on all network interfaces.
        server.listen(port);
        server.on('error', onError);
        server.on('listening', function ()
        {
            onListening(server);
        });
        module.exports = app;
    });
}
//Normalize a port into a number, string, or false.
function normalizePort(val)
{
    var port = parseInt(val, 10);
    if(isNaN(port))
    {
        // named pipe
        return val;
    }
    if(port >= 0)
    {
        // port number
        return port;
    }
    return false;
}
//Event listener for HTTP server "error" event.
function onError(error)
{
    console.log(error);
    if(error.syscall !== 'listen')
    {
        throw error;
    }
    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
    // handle specific listen errors with friendly messages
    switch(error.code)
    {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}
//Event listener for HTTP server "listening" event.
function onListening(server)
{
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    console.log('Listening on ' + bind);
}

