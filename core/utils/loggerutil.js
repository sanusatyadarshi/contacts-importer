var bunyan = require('bunyan');
var util = bunyan.createLogger({
    name: 'Logger',
    serializers: bunyan.stdSerializers,
    streams: [
			{
				stream: process.stdout,
				level: 'info'
			},
			{
				stream: process.stderr,
				level: 'fatal'
			}
		]
});

module.exports = util;
