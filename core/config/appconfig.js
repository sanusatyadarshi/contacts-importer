var events = require('events');

var config = {
    name: "contact-importer",
    errorsrcfile: 'logs/error.log',
    infosrcfile: 'logs/info.log',
}

config.emitter = new events.EventEmitter();
config.updateCallback = function (strPropertyName) 
{
    console.log("+++++++++++++++AppConfig updateCallback++++++++++++++++++++");
    console.log(strPropertyName);
    console.log(config[strPropertyName]);
    console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

    
    config.emitter.emit(strPropertyName + "Changed");
};


module.exports = config;
