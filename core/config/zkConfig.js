var appConfig = require('./appconfig');
var cloudSpongeConfig = require('./cloudspongeconfig');

var zkRoot = process.env.ZK_ROOT || "FE"
var config = {
    //ZK sever config
    /**
     * Put in your config overrides here.
     * Precedence : UUID > local > global 
     **/

    // Allowed type for global configuration is 
    // int, float, string, object

    //Global properties.
    global: [
    {
        path: `/${zkRoot}/global/commonRedisClusterNodes`,
        target: appConfig,        
        propertyName: 'redisClusterNodes',
        type: "object"
    }],
    //Local properties.
    basePath: `/${zkRoot}/contact-importer`, //Base path required for fetching ZK nodes

    local: [
    {
        path: '/appconfig',
        target: appConfig
    }, {
        path: '/cloudsponge',
        target: cloudSpongeConfig
    }]
}

module.exports = config;
