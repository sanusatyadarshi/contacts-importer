/**
 ** Module: Redis cache service.
 **/

var Redis = require('ioredis');

var appConfig = require('../config/appconfig');

var logger = require('../utils/loggerutil');

var service = {};

var cluster = null;

var currRedisNodes = null;
var currRedisNodes = appConfig.redisClusterNodes.hosts;

appConfig.emitter.on('redisClusterNodesChanged', function() {
    //Will serve pending request before quiting.
    console.log('REDIS: Received update of nodes.', appConfig.redisClusterNodes.hosts);
    logger.info(appConfig.redisClusterNodes.hosts, 'REDIS: Revieved update of nodes.');
    service._init();
});

service._init = function() {
    var cluster1 = new Redis.Cluster(appConfig.redisClusterNodes.hosts, {
        showFriendlyErrorStack: true
    });
    cluster1.on('connect', function() {
        logger.info('REDIS: Cluster connected.');
        console.log('REDIS connected .... to ', appConfig.redisClusterNodes.hosts);
        //Refer cluster1 globally.
        if (cluster) {
            cluster.disconnect();
        }
        cluster = cluster1;
        /* Listen to redis change events.
         * Quit after port/host change
         */
    });

    cluster1.on('close', function() {
        console.log('REDIS closing ......to ', currRedisNodes);
        currRedisNodes = appConfig.redisClusterNodes.hosts;
        logger.info('REDIS: Disconnected from the server.');
    });

    cluster1.on('reconnecting', function() {
        logger.info('REDIS: Trying to connect to a redis server.');
    });

    cluster1.on('error', function(err) {
        logger.error(err, 'REDIS: Connection failed due an Error.');
    });

    cluster1.on('end', function() {
        logger.info('REDIS: Connection was ended from server.');
        disconnected = true;
    });
};

/*service._update = function(data) {
    console.log(' >>>>>> ',AppConfig.redisClusterNodes.hosts);
    //AppConfig.redisClusterNodes.hosts = data.redisNodes;
    service._init();
};*/

service.setData = function(key, value, callback) {
    //Check: cluster is available or not.
    if (Object.keys(cluster).length) {
        cluster.set(key, value, function(err, result) {
            if (err) {
                logger.error(err, 'REDIS: Error in setting the provided key into server.');
                return callback(err);
            }
            logger.info(key, 'REDIS: Key set in the server.');
        });
        callback(null);
    }
};

service.getData = function(key, callback) {

    cluster.get(key, function(err, result) {
        if (err) {
            logger.error(err, 'REDIS: Some error in retreiving key from server.');
            return callback(err); // Return err if key was not available or not retrieved.
        }
        logger.info(result, 'REDIS: Key retrieved from the server.');
        callback(null, result);
    });

};

service.deleteData = function(key, callback) {
    try {
        cluster.del(key);
        callback(null);
    } catch (err) {
        logger.error(err, 'REDIS: Some error in deleting key from server.');
        return callback(err);
    }
};

service._init();

module.exports = service;
