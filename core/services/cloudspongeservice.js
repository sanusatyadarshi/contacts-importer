var request = require('superagent');
var cloudspongeConfig = require('../config/cloudspongeconfig');
var logger = require('../utils/loggerutil.js');
var AUTH = {
    domain_key: cloudspongeConfig.DOMAIN_KEY,
    domain_password: cloudspongeConfig.DOMAIN_PASSWORD
};
var service = {
    getConsent: function (serviceName, callback)
    {
        var auth = JSON.parse(JSON.stringify(AUTH)); // Extend object(copy) without reference.
        auth.service = serviceName;
        auth.include = ['mailing_address', 'name', 'email'];
        callback = callback || function () {};
        logger.info(
        {
            data: serviceName
        }, 'Received Get Consent Url request from client.');
        request.post(cloudspongeConfig.urls.consent + '.' + cloudspongeConfig.dataType).send(auth).end(function (err, result)
        {
            if(err)
            {
                logger.error(
                {
                    error: err
                }, 'Error: Failed to get the Consent Url response from cloud-sponge');
                callback(err);
                return;
            }

            if(result && result.body && result.body.error)
            {
                logger.error(
                {
                    error: result.body
                }, 'Error: Failed to get the Consent Url response from cloud-sponge');
                callback(result.body);
                return;
            }

            logger.info(
            {
                data: result.body
            }, 'Success: Received consent url response from cloud-sponge.');
            callback(null, result.body);
        });
    },
    checkImport: function (import_id, callback)
    {
        var auth = JSON.parse(JSON.stringify(AUTH)); // Extend object(copy) without reference.
        auth.import_id = import_id;
        auth.domain_key = cloudspongeConfig.DOMAIN_KEY;
        callback = callback || function () {};
        logger.info(
        {
            data: import_id
        }, 'Received Check Import request from client.');
        request.get(cloudspongeConfig.urls.events + import_id + '.' + cloudspongeConfig.dataType).query(auth).end(function (err, result)
        {
            if(err)
            {
                logger.error(
                {
                    error: err
                }, 'Error: Failed to get the Check Import Url response from cloud-sponge');
                callback(err);
                return;
            }
            
            if(result && result.body && result.body.error)
            {
                logger.error(
                {
                    error: result.body
                }, 'Error: Failed to get the Check Import Url response from cloud-sponge');
                callback(result.body);
                return;
            }

            logger.info(
            {
                data: result.body
            }, 'Success: Received Check Import response from cloud-sponge.');
            callback(null, result.body);
        });
    },
    getContacts: function (import_id, callback)
    {
        var auth = JSON.parse(JSON.stringify(AUTH)); // Extend object(copy) without reference.
        auth.import_id = import_id;
        callback = callback || function () {};
        logger.info(
        {
            data: import_id
        }, 'Received Get Contacts request from client.');
        request.get(cloudspongeConfig.urls.contacts + import_id + '.' + cloudspongeConfig.dataType).query(auth).end(function (err, result)
        {
            if(err)
            {
                logger.error(
                {
                    error: err
                }, 'Error: Failed to get the Get Contacts Url response from cloud-sponge');
                callback(err);
                return;
            }

            if(result && result.body && result.body.error)
            {
                logger.error(
                {
                    error: result.body
                }, 'Error: Failed to get the Get Contacts Url response from cloud-sponge');
                callback(result.body);
                return;
            }

            logger.info(
            {
                data: result.body
            }, 'Success: Received Get Contacts response from cloud-sponge.');
            callback(null, result.body);
        });
    }
}
module.exports = service;