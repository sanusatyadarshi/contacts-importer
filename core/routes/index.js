var express = require('express');
var router = express.Router();

var api = require('../controllers/apicontroller');

router.get('/getconsenturl', api.getConsentUrl);
router.get('/checkImport', api.checkImportComplete);
router.get('/downloadcontacts', api.getAddressBook);
router.get('/deleteToken', api.clearRedisToken);

module.exports = router;
