var os = require('os');

var getmac = {}
getmac.GetMacAddress = function()
{
	var objInterfaces = os.networkInterfaces();
	var arrPreferances = ["eth", "wlan", "lo", ""]

	for(var j = 0; j < arrPreferances.length; j++)
	{
		for(key in objInterfaces)
		{
			if(key.toLowerCase().indexOf(arrPreferances[j]) == 0)
			{
				var arrAddress = objInterfaces[key];

				for(var i = 0; i < arrAddress.length; i++)
				{
					var address = arrAddress[i]
					if(address.mac != "00:00:00:00:00:00" && !address.internal)
					{
						return address.mac;
					}
				}
			}
		}
	}
	return false;
}

console.log("MacAddress: " + getmac.GetMacAddress());

module.exports = getmac;
