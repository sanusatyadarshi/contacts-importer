var cloudsponge = require('../services/cloudspongeservice'),
    redis = require('../services/redisservice');
var appConfig = require('../config/appconfig');
var logger = require('../utils/loggerutil'),
    randomid = require('../utils/randomidutil');

var util = {};
util.flattenContactData = function(contacts)
{
    var flattenData = {
        contacts: {},
        emailMap: {},
        numbersMap: {}
    };

    if(contacts && Array.isArray(contacts) && contacts.length)
    {
        for (var i = contacts.length - 1; i >= 0; i--)
        {
            var contact = contacts[i]
            if(contact)
            {
                flattenData.contacts[i] = contact;
                if(contact.email && contact.email.length)
                {
                    for (var j = contact.email.length - 1; j >= 0; j--)
                    {
                       if(contact.email[j].address) flattenData.emailMap[contact.email[j].address] = i;
                    }
                }

                if(contact.phone && contact.phone.length)
                {
                    for (var j = contact.phone.length - 1; j >= 0; j--)
                    {
                       if(contact.phone[j].number) flattenData.numbersMap[contact.phone[j].number] = i;
                    }
                }

                flattenData.contacts[i].email = [];
                flattenData.contacts[i].phone = [];
            }
        }
    }

    return flattenData;
}

var controller = {
    // Returns OAUTH url for email provider.
    getConsentUrl: function (req, res, next)
    {
        var emailDomain = req.query.serviceName;
        cloudsponge.getConsent(emailDomain, function (err, result)
        {
            if(err)
            {
                return next(err);
            }
            var token = appConfig.tokenPrefix + randomid.generate(appConfig.tokenLength);
            redis.setData(token, result.import_id, function (redisErr, redisResult)
            {
                if(redisErr)
                {
                    return next(redisErr);
                }

                logger.info(
                {
                    data: result
                }, 'User consent URL has been sent to the client.');

                res.send(
                {
                    token: token,
                    url: result.url,
                    status: result.status
                });
            });
        });
    },
    //Checks whether the import has been successfull or failed.
    checkImportComplete: function (req, res, next)
    {
        var token = req.query.token;
        redis.getData(token, function (err, result)
        { // RESULT = cloud-sponge import_id.
            if(err)
            {
                return next(err);
            }
            var import_id = result;
            var COMP_EVENT_NAME = 'COMPLETE',
                COMP_EVENT_STATUS = 'COMPLETED',
                ERROR_EVENT_STATUS = 'ERROR',
                timeoutCounter = 0;

            function callImportApi()
            {
                cloudsponge.checkImport(import_id, function (csErr, csResult)
                {
                    timeoutCounter++;
                    var importComplete = false, errorOccurred = false;
                    if(csErr || !csResult)
                    {
                        logger.info(csErr, "Error returned by check Import Cloud Sponge API");
                        if(csErr && csErr.message && csErr.message === 'Unauthorized')
                        {
                            res.send(
                            {
                                importCompleted: false
                            });
                            return;
                        }
                        if(csErr && csErr.status && csErr.status == 401)
                        {
                            res.send(
                            {
                                importCompleted: false
                            });
                            return;
                        }
                    }
                    else
                    {
                        var temp = csResult.events.filter(function (obj)
                        {
                            if(obj.status === ERROR_EVENT_STATUS)
                            {
                                errorOccurred = true;
                            }
                            else if(obj.event_type === COMP_EVENT_NAME && obj.status === COMP_EVENT_STATUS)
                            {
                                importComplete = true;
                            }
                        })[0]; // First index contains the filtered object with COMPLETE event.
                    }
                    if(errorOccurred || timeoutCounter >= 10)
                    {
                        // Check for multiple running instances of importTimer
                        if(!res.headersSent)
                        {
                            res.send(
                            {
                                importCompleted: false
                            });
                        }
                    }
                    else if(importComplete)
                    {
                        if(!res.headersSent)
                        {
                            console.log('sending response now .....')
                            res.send(
                            {
                                importCompleted: true
                            });
                        }
                    }
                    else
                    {
                        setTimeout(callImportApi, 1000);
                    }
                });
            };
            setTimeout(callImportApi, 1000);
        });
    },
    getAddressBook: function (req, res, next)
    {
        var token = req.query.token;
        var flatten = req.query.flatten;

        redis.getData(token, function (err, result)
        {
            if(err)
            {
                return next(err);
            }
            
            var import_id = result;
            cloudsponge.getContacts(import_id, function (err, contacts)
            {
                contacts = contacts || {};
                // Check for multiple running instances of importTimer
                logger.info({flatten: flatten}, 'Contacts sent to the client.');
                if(flatten)
                {
                    var data = util.flattenContactData(contacts.contacts);
                    res.send(data);
                }
                else
                {
                    res.send(contacts);
                }
            });
        });
    },
    // Delete token from redis.
    clearRedisToken: function (req, res, next)
    {
        var token = req.query.token;
        redis.deleteData(token, function (err)
        {
            if(err)
            {
                return next(err);
            }
            console.log('Token deleted from redis.')
            res.status(200).send('Deleted');
        });
    }
}
module.exports = controller;